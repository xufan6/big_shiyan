#!/bin/bash
cd `dirname $0`

cd daidan
../plot.sh CMC-0.25% CMC-0.50% CMC-0.75% CMC-1.00% 1.4 cmc CMC
cd ../

cd hjc
../plot.sh 黄原胶-0.25% 黄原胶-0.50% 黄原胶-0.75% 黄原胶-1.00% 2.5 hyj 黄原胶
cd ../

cd www
../plot.sh 卡拉胶-0.25% 卡拉胶-0.50% 卡拉胶-0.75% 卡拉胶-1.00% 0.49 klj 卡拉胶
cd ../

cd slf
../plot.sh 琼脂-0.25% 琼脂-0.50% 琼脂-0.75% 琼脂-1.00% 3.5 qz 琼脂
cd ../
