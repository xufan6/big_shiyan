#!/bin/bash
if [ "$#" -lt "7" ] ; then
    echo "Usage, \$1~\$4 is datafile, \$5 is yrange,"
    echo "       \$6 is {output name}.png, \$7 is pic's title"
    exit 3
fi
gnuplot << EOF
set terminal pngcairo size 800,600
set output "$6.png"
set title "$7"
set key box
set grid xtics ytics mxtics
set xrange [ 0 : 120 ] noreverse nowriteback
set yrange [ 0 : $5 ] noreverse nowriteback
set mxtics 2
set xlabel "D[1/s]"
set ylabel "Eta[Pas]"
plot '$1' notitle with points pt 7 lt -1, '$1' smooth bezier with lines lt -1,\
     '$2' notitle with points pt 7 lt  1, '$2' smooth bezier with lines lt  1,\
     '$3' notitle with points pt 7 lt  2, '$3' smooth bezier with lines lt  2,\
     '$4' notitle with points pt 7 lt  3, '$4' smooth bezier with lines lt  3
EOF
