#!/bin/bash
while (( "$#" )); do
awk -F"[; \t]" '{print $10,$12}' $1 |tr -d \" |tr -s , .|tail -n +2 > ${1%.*}
shift
done
